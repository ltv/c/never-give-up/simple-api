const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const PORT = 5000;

// app settings
app.set("view engine", "ejs");
app.set("views", "./views");
app.use(express.static("public"));
app.use(bodyParser.json());
// and app settings

app.use("/", (req, res) => {
  res.json({ message: "OK" });
});

app.listen(PORT, () => {
  console.log(`Server run on port ${PORT}`);
});
